import smtplib
from email.mime.text import MIMEText
from Control_data import DATA
# from kavenegar import *
import requests
import json
import khayyam
from Data.DataBase import insert_money
from decimal import Decimal

ss = requests.get('https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/usd/irr.json').text
ss1 = requests.get('https://serpapi.com/searches/34acddb1d9884194/65ad0b0c9bddf70c4ad6beac.json').text
ss2 = (requests.get
       ('https://call1.tgju.org/ajax.json?rev=LBEFXdN6zwV0QUvtbHGZlZEArKrGv9Hdb9amGOS6kwza5T3Oo1qCdATQGihB').text)

ss33 = json.loads(ss2)
prise = ss33['current']['crypto-usd-coin-irr']['p']
date_prise = ss33['current']['crypto-usd-coin-irr']['t_en']
json_data = json.loads(ss1)['markets']['crypto'][0]['price']
convert = prise.replace(',', '.')
convert_to_float = '{0:.3f}'.format(float(convert))
json_converted = json.loads(ss)['irr']
Date = khayyam.JalaliDatetime.now().strftime('%Y-%m-%d %H:%M:%S')
msg_send = (f'قیمت دلار =  {prise} \n بروز شده در ساعت =  {date_prise} \n قیمت بیت کوین به دلار = {Date}'
            f' \n {Date}')


def send_mail():
    to = 'yasinmonfard@gmail.com'
    subject = 'USD SHOW'
    # msg = input('Enter a message: ')
    fr = DATA['send_mail']['from']
    passw = DATA['keys']['email']
    msg_ = MIMEText(msg_send)
    msg_['From'] = fr
    msg_['Subject'] = subject
    msg_['To'] = to

    with smtplib.SMTP('smtp.gmail.com', 587) as server:
        server.starttls()
        server.login(fr, passw)
        server.sendmail(fr, to, msg_.as_string())
        server.quit()
        print('Email sent!')


if __name__ == '__main__':
    pass
    if DATA['send_mail']['enable']:
        pass
        send_mail()
        insert_money(money=convert_to_float,
                     date=Date)

# api = KavenegarAPI('52427766684A6A2F4C6A58667347474433764767766E755074496A68456C39695A7750356A5A69486562513D')
# params = {'sender': '2000500666',
#           'receptor': '09337472986',
#           'message': msg}
# response = api.sms_send(params)
